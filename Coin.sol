pragma solidity >=0.5.0 <0.6.3;

// import "./IERC20.sol";
// import "../../math/SafeMath.sol";

contract Coin{

    using SafeMath for uint256;
    //
   // 0x14723A09ACff6D2A60DcdF7aA4AFf308FDDC160C //ran
   // 0x14723A09ACff6D2A60DcdF7aA4AFf308FDDC160B //kaue
    address public admin;
    mapping ( address => uint256) private wallet;
    
    constructor() public{
        //msg
        admin = msg.sender;
    }
    // uint (integer or decimal numbers)
    //Mint is creation of internal value unit
    function mint(uint256 _amount) public  {
        require(msg.sender==admin,"YOU ARE NOT ALLOWED TO MINT");
          wallet[ msg.sender] += _amount;
        //   wallet = wallet + amount;
        //100,000 
          
    }
    
    //Transfer is when a mint is completed, use it to transfer to another account
    function transfer(address _to, uint256 _amount) public{
        //Assignment only transfer positive values && amount not greater than sender wallet value
        
        wallet[_to] += _amount;
        wallet[msg.sender] -= _amount;
        
    }
    
    function burn(uint256 _amount) public{
        //Assignment check always the burn amount is less than or equal to total amount minted.
        //Positive values
        wallet[msg.sender]-=_amount;
    }
    
    function checkValue(address _address) public view returns (uint256){
        //Assignment only the user can check his balance or the admin
        return wallet[_address];
    }
    
    
    
}
