pragma solidity >=0.5.0 <0.6.3;

contract GaiaAdmins{
    
    enum ADMINROLE { REQUESTOR, APPROVER}
    enum ADMINSTATUS { ACTIVE, PENDING , FREEZE }
    
 
    
    struct Admin{
        ADMINROLE _adminRole;
        ADMINSTATUS _adminStatus;
        address _pub;
        uint256 _index;
        
        
        
    }

    
    mapping ( address => Admin) private adminsStruct;
    address [] private adminsIndex; 
    
    
    constructor(address _approver) public{
        address _requester = msg.sender;
        createRequestorAdmin(_requester,_approver);
        
    }
    
    function createRequestorAdmin(address _requester, address _approver)private  returns {
        
        adminsStruct[_requester]._adminRole = ADMINROLE.REQUESTOR;
        
         
        //  return adminsIndex.length-1;
    }
    
}
