pragma solidity >=0.5.0 <0.6.3;
import "GaiaNaming.sol";

contract GaiaCoin{
    
    enum GaiaCoinEnum {OK, PENDING, FREEZE}
    
    
    
    
    struct Pending{
        address[] _amount;
        
    }
    
    
    
  
    address public naming;
    address private minter;
    string private constant currency="PHP";
    mapping ( address => uint) private balances;
    
    event Sent(address _from, address _to, uint amount);
    
    constructor(address _naming) public{
        minter = msg.sender;
        naming=_naming;
    }
    
    
    modifier onlyMinter() {
        require (msg.sender==minter,"Permission denied");
        _;
    }
    
    function mint(address _receiver, uint _amount, string memory _transaction,string memory _method) public onlyMinter{
        //  require(msg.sender==minter,"Permission denied");
         require(_amount<1e60,"Amount invalid");
         require(_amount>0,"Amount invalid");
         
         bytes memory tempEmptyTransactionTest = bytes(_transaction); 
         bytes memory tempEmptyMethodTest = bytes(_method);
         
         require(tempEmptyTransactionTest.length>0,"Transaction number is needed");
         require(tempEmptyMethodTest.length>0,"Payment method is needed");
         
         
         balances[ _receiver] += _amount;
    }
    
    
    
    function burn(address _account, uint _amount,string memory, string memory _transaction,string memory _method) public{
         require(msg.sender==minter,"Permission denied");
         require(_amount<1e60,"Amount invalid");
         
         bytes memory tempEmptyTransactionTest = bytes(_transaction); 
         bytes memory tempEmptyMethodTest = bytes(_method);
         
         require(tempEmptyTransactionTest.length>0,"Transaction number is needed");
         require(tempEmptyMethodTest.length>0,"Payment method is needed");
         
         balances[ _account] -= _amount;
         
    }

    function checkBalance(address _account) public view returns (uint){
        require(msg.sender==minter || msg.sender==_account,"Permission denied");
        return balances[_account];
    }    
    
    
    function retrieveCurrency() public pure returns (string memory){
       
        return currency;
    } 
    
    function checkCustomerCount() public view returns(uint256){
           GaiaNaming gn = GaiaNaming(address(naming));
           return gn.getCustomerCount();
    }
    
    function transfer(address _to, uint _amount) public{
        require(_amount<=balances[msg.sender],"Not Enough Funds");
        
        balances[msg.sender] -= _amount;
        balances[_to] += _amount;
        
        emit Sent(msg.sender, _to, _amount);
    }

}
