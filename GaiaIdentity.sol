pragma solidity >=0.5.0 <0.6.3;


contract GaiaIdentity{
    
    address private admin;
    // GaiaNaming naming;
    
    
    constructor() public{
        admin = msg.sender;
    }
    
    function hashSeriesNumber(address series, uint256 number) external pure returns (address)
    {
    string memory tempSeries = toString(series);
    bytes32 tempData = keccak256(abi.encode(number, tempSeries));
    return address(uint160(uint256(tempData)));
    }
    

    
    function toString(address x) internal pure returns (string memory) {
    bytes memory b = new bytes(20);
    for (uint i = 0; i < 20; i++)
        b[i] = byte(uint8(uint(x) / (2**(8*(19 - i)))));
    return string(b);
}

// function addressToString(address _addr) public pure returns(string) {
//     bytes32 value = bytes32(uint256(_addr));
//     bytes memory alphabet = "0123456789abcdef";

//     bytes memory str = new bytes(42);
//     str[0] = '0';
//     str[1] = 'x';
//     for (uint i = 0; i < 20; i++) {
//         str[2+i*2] = alphabet[uint(uint8(value[i + 12] >> 4))];
//         str[3+i*2] = alphabet[uint(uint8(value[i + 12] & 0x0f))];
//     }
//     return string(str);
// }


}
