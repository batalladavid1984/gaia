pragma solidity >=0.5.0 <0.6.3 ;
import "GaiaIdentity.sol";

contract GaiaNaming{
    

     enum  CustomerStatus { ACTIVE, FREEZE, PENDING }
     enum  CustomerUpdate { ACTIVATE, LOCK, CHANGEPUBKEY}
    
 
    address private admin;
    address public token;
    
    constructor() public{
        admin = msg.sender;
        // token = _token;
    }
    
    


    
    struct Customer{
        address _pub;
        address _identity;
        CustomerStatus _status;
        address _kycID;
        uint256 _index;
    }
    
        struct Identity{
        address _identity;
        uint256 _index;
    }
    

    

    
    
     
    
    mapping ( address => Customer) private customersStruct;
    mapping ( address => Identity ) private identityStruct;
    
    address[] private userIndex;
    address[] private identityIndex;
  
   

    
    
  event NewUser( uint256 index, address indexed _pub, CustomerStatus _status,address _kycID, address _identity);
  event NewIdentity(uint256 index, address  _identity);
  event UpdateUser(uint256 index, address indexed _pub, CustomerStatus _status,address _kycID, address _identity);
    
    
    function createCustomer( address _pub, address _kycID) public  returns (uint256 index){
     

    require(isUser(_pub)==false,"Duplicate Customer Public Key");
    address tempIden = createIdentity(_pub);
    address _identity = tempIden;
    require(isIdentity(tempIden)==false,"Duplicate Customer Identity ");
    
    customersStruct[_pub]._pub = _pub;
    customersStruct[_pub]._status   = CustomerStatus.PENDING;
    customersStruct[_pub]._kycID = _kycID;
    customersStruct[_pub]._identity = tempIden;
    customersStruct[_pub]._index = userIndex.push(_pub);
    
    identityStruct[_identity]._identity = _identity;
    identityStruct[_identity]._index = identityIndex.push(_identity)-1;

    emit    NewUser(customersStruct[_pub]._index, customersStruct[_pub]._pub, customersStruct[_pub]._status,customersStruct[_pub]._kycID ,
                    customersStruct[_pub]._identity);
                    
    emit NewIdentity(identityStruct[_identity]._index, identityStruct[_identity]._identity);
    
    return userIndex.length-1;
            
    }
    
    function freeze(address _pub)public returns (bool){
        require(customersStruct[_pub]._kycID==msg.sender,"Invalid KYC");
        customersStruct[_pub]._status=CustomerStatus.FREEZE;
        emit UpdateUser(customersStruct[_pub]._index, customersStruct[_pub]._pub, customersStruct[_pub]._status,customersStruct[_pub]._kycID, customersStruct[_pub]._identity);
        
        return true;
    }
    
    function activate(address _pub) public returns (bool){
         require(customersStruct[_pub]._kycID==msg.sender,"Invalid KYC");
         require(isUser(_pub)==true,"Invalid Public Key");
         customersStruct[_pub]._status=CustomerStatus.ACTIVE;
         emit UpdateUser(customersStruct[_pub]._index, customersStruct[_pub]._pub, customersStruct[_pub]._status,customersStruct[_pub]._kycID, customersStruct[_pub]._identity);
        return true;
    }
    
    
    function createIdentity(address _pub) internal  returns(address){
         GaiaIdentity giden = new GaiaIdentity();
         return giden.hashSeriesNumber(_pub,getCustomerCount());
    }
    
    
    function getCustomerCount()  public view returns (uint256){
        return userIndex.length;
    }
    

    
    
    function isUser(address _pub)
    public view
    returns (bool isIndeed)
    {
        
            if(userIndex.length == 0) return false;
                return (userIndex[_pub] == _pub);
    }
    
    function isIdentity(address _identity)
    public view
    returns (bool isIndeed)
    {
        
            if(identityIndex.length == 0) return false;
                return (identityIndex[identityStruct[_identity]._index] == _identity);
    }
    

    
    function getUser(address userAddress)
    public 
    view
    returns(address _pub, uint256 _index, CustomerStatus _status,address _kycID,address _identity)
  {
    
    require(!isUser(userAddress)==false);
    return(
      customersStruct[userAddress]._pub, 
      customersStruct[userAddress]._index, 
      customersStruct[userAddress]._status,
      customersStruct[userAddress]._kycID,
      customersStruct[userAddress]._identity
      );
  } 
    
    function getIdentity(address identityAddress)
    external
    view
    returns( uint256 _index, address _identity){
            require(!isIdentity(identityAddress)==false);
    return(
    
      identityStruct[identityAddress]._index, 
 
      identityStruct[identityAddress]._identity
      );
    }
    
    function getUserAtIndex(uint256 index)
    public
    view
    returns(address userAddress)
  {
    return userIndex[index];
  }

    
    
}


